<?php

// Keep WPMU logged in

define( 'WPMUDEV_APIKEY' , 'c8dd7181094289a9abdb3eb594817ee1a797a5c5');

// Add custom excerpt length echo $excerpt(20);

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

// Add custom image sizes. change image size name to fit your need

//add_image_size( 'attorney-small', 350, 350, TRUE );
