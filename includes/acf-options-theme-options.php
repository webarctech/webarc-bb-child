<?php
/*
 * This is only used if we want to include turning on and off custom post types
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}
/*
if( function_exists('acf_add_local_field_group') ) {

	acf_add_local_field_group(array (
		'key' => 'group_1',
		'title' => 'Custom Post Types',
		'fields' => array (
			array (
				'key' => 'field_1',
				'label' => 'Custom Post Types',
				'name' => 'custom_post_types',
				'type' => 'checkbox',
				'prefix' => '',
				'instructions' => 'Select The Custom Post Types To Include In Theme',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
				'choices' => array(
					'team'	=> 'Team',
					'video' => 'Video'
				),
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'theme-general-settings',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));
}
*/
/*
 * If this is being used then add the following code to the functions.php
 */
/*
if( function_exists('acf_add_options_page') ) {
    $field = get_field_object( 'custom_post_types', 'options' );
}

if (!empty($field)) {
    $postTypes = $field['value'];

    if ( in_array( "team", $postTypes ) ) {
        require_once 'includes/post-types/cpt-team.php';
    }

    if ( in_array( "video", $postTypes ) ) {
        require_once 'includes/post-types/cpt-video.php';
    }
}
*/