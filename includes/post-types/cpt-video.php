<?php
// Register Custom Post Type
function video_cpt() {

	$labels = array(
		'name'                  => _x( 'Videos', 'Post Type General Name', 'fl-automator' ),
		'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'fl-automator' ),
		'menu_name'             => __( 'Videos', 'fl-automator' ),
		'name_admin_bar'        => __( 'Video', 'fl-automator' ),
		'archives'              => __( 'Video Archives', 'fl-automator' ),
		'attributes'            => __( 'Video Attributes', 'fl-automator' ),
		'parent_item_colon'     => __( 'Parent Video:', 'fl-automator' ),
		'all_items'             => __( 'All Videos', 'fl-automator' ),
		'add_new_item'          => __( 'Add New Video', 'fl-automator' ),
		'add_new'               => __( 'Add New', 'fl-automator' ),
		'new_item'              => __( 'New Video', 'fl-automator' ),
		'edit_item'             => __( 'Edit Video', 'fl-automator' ),
		'update_item'           => __( 'Update Video', 'fl-automator' ),
		'view_item'             => __( 'View Video', 'fl-automator' ),
		'view_items'            => __( 'View Videos', 'fl-automator' ),
		'search_items'          => __( 'Search Videos', 'fl-automator' ),
		'not_found'             => __( 'Not found', 'fl-automator' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'fl-automator' ),
		'featured_image'        => __( 'Featured Image', 'fl-automator' ),
		'set_featured_image'    => __( 'Set featured image', 'fl-automator' ),
		'remove_featured_image' => __( 'Remove featured image', 'fl-automator' ),
		'use_featured_image'    => __( 'Use as featured image', 'fl-automator' ),
		'insert_into_item'      => __( 'Insert into Video', 'fl-automator' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Video', 'fl-automator' ),
		'items_list'            => __( 'Videos list', 'fl-automator' ),
		'items_list_navigation' => __( 'Video list navigation', 'fl-automator' ),
		'filter_items_list'     => __( 'Filter Videos list', 'fl-automator' ),
	);

	$rewrite = array(
		'slug' => 'Video',
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	);

	$args = array(
		'label'                 => __( 'Video', 'fl-automator' ),
		'description'           => __( 'Video posts', 'fl-automator' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
		'taxonomies'            => array( ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-video',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite' => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'video', $args );

}
add_action( 'init', 'video_cpt', 0 );
