<?php
// Register Custom Post Type
function team_cpt() {

    $labels = array(
        'name'                  => _x( 'Team', 'Post Type General Name', 'fl-automator' ),
        'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'fl-automator' ),
        'menu_name'             => __( 'Team', 'fl-automator' ),
        'name_admin_bar'        => __( 'Team', 'fl-automator' ),
        'archives'              => __( 'Team Archives', 'fl-automator' ),
        'attributes'            => __( 'Team Attributes', 'fl-automator' ),
        'parent_item_colon'     => __( 'Parent Member:', 'fl-automator' ),
        'all_items'             => __( 'All Members', 'fl-automator' ),
        'add_new_item'          => __( 'Add New Member', 'fl-automator' ),
        'add_new'               => __( 'Add New', 'fl-automator' ),
        'new_item'              => __( 'New Member', 'fl-automator' ),
        'edit_item'             => __( 'Edit Member', 'fl-automator' ),
        'update_item'           => __( 'Update Item', 'fl-automator' ),
        'view_item'             => __( 'View Item', 'fl-automator' ),
        'view_items'            => __( 'View Items', 'fl-automator' ),
        'search_items'          => __( 'Search Item', 'fl-automator' ),
        'not_found'             => __( 'Not found', 'fl-automator' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'fl-automator' ),
        'featured_image'        => __( 'Featured Image', 'fl-automator' ),
        'set_featured_image'    => __( 'Set featured image', 'fl-automator' ),
        'remove_featured_image' => __( 'Remove featured image', 'fl-automator' ),
        'use_featured_image'    => __( 'Use as featured image', 'fl-automator' ),
        'insert_into_item'      => __( 'Insert into team', 'fl-automator' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'fl-automator' ),
        'items_list'            => __( 'Team list', 'fl-automator' ),
        'items_list_navigation' => __( 'Team list navigation', 'fl-automator' ),
        'filter_items_list'     => __( 'Filter teams list', 'fl-automator' ),
    );

    $rewrite = array(
        'slug' => 'team-member',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );

    $args = array(
        'label'                 => __( 'team', 'fl-automator' ),
        'description'           => __( 'team member posts', 'fl-automator' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
        'taxonomies'            => array( ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-businessman',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite' => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'team', $args );

}
add_action( 'init', 'team_cpt', 0 );
