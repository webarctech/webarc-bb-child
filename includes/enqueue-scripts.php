<?php
// Enqueue scripts

// Update CSS within in Admin
function admin_style() {
    wp_enqueue_style('admin-styles', FL_CHILD_THEME_URL.'/dist/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');


function site_scripts() {
    // comment this out and remove variable from scripts before going live
    $_bust_cache = rand(10,100);

    // Custom JS
    wp_enqueue_script( 'theme-custom-js', FL_CHILD_THEME_URL. '/dist/js/custom.js', array( 'jquery' ), $_bust_cache , true );

    // Register stylesheets
    wp_enqueue_style( 'override-css', FL_CHILD_THEME_URL. '/dist/css/custom.css', array(), $_bust_cache , 'all' );

}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

// Enqueue Dashicons 
add_action( 'wp_enqueue_scripts', 'enqueue_dashicons' );
function enqueue_dashicons() {
    wp_enqueue_style( 'dashicons' );
}

