<?php
// Custom Fonts
function webarc_custom_fonts ( $system_fonts ) {
    /* Change the system font name to the name of the font you're using.
     * Note you will need to add an @import in the custom.css file.
     * You may also not have as many or more font weights. Reference this doc for assistance if necessary
     * https://docs.wpbeaverbuilder.com/bb-theme/defaults-for-styles/typography/add-web-fonts-to-your-theme-and-the-beaver-builder-plugin/
     * Or https://docs.wpbeaverbuilder.com/bb-theme/defaults-for-styles/typography/add-web-fonts-complex-example
     * Also note that this should be disabled unless being used. Otherwise it will add these fonts as possibilities in the BB admin.
    */
    $system_fonts[ 'Trajan Pro 3' ] = array(
        'fallback' => 'Verdana, Arial, sans-serif',
        'weights' => array(
            '400',
            '700',
        ),
    );

    $system_fonts[ 'Avenir Book' ] = array(
        'fallback' => 'Verdana, Arial, sans-serif',
        'weights' => array(
            '400',
        ),
    );
    $system_fonts[ 'Avenir' ] = array(
        'fallback' => 'Verdana, Arial, sans-serif',
        'weights' => array(
            '100',
            '300',
            '400',
            '500',
            '700',
            '900'
        ),
    );

    return $system_fonts;
}

//Add to Beaver Builder Theme Customizer
add_filter( 'fl_theme_system_fonts', 'webarc_custom_fonts' );

//Add to Beaver Builder modules
add_filter( 'fl_builder_font_families_system', 'webarc_custom_fonts' );