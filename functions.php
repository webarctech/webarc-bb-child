<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'fl_head', 'FLChildTheme::stylesheet' );

//require_once 'includes/acf-options.php';
require_once 'includes/enqueue-scripts.php';
require_once 'includes/helpers.php';
require_once 'includes/shortcodes.php';
//require_once 'includes/post-types/cpt-team.php';
//require_once 'includes/post-types/cpt-video.php';
//require_once 'includes/post-types/cpt-drinks.php';
/* The fonts.php should only be included if there are custom fonts. Set the fonts you want to include
 * in fonts.php before uncommenting the line below
*/
//require_once    'includes/fonts.php';

